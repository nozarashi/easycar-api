const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();

//static folder
app.use(express.static('public'));

require('./helpers/cron-tasks');
//Load routes
const users = require('./routes/users');
const cars = require('./routes/cars');
const travels = require('./routes/travels');
const travelTypes = require('./routes/travelTypes');

//Load keys
const keys = require('./config/keys');

mongoose.Promise = global.Promise;
mongoose.connect(keys.mongoURI, {
  useMongoClient: true
}).then(
  () => console.log('MongoDB Connected...'),
  (err) => console.log(err));

//CORS middleware
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
  res.header("Allow-Access-Expose-Headers", "x-auth");
  res.header("Access-Control-Expose-Headers", "content-disposition, x-auth");

  next();
});



//BodyParser Middleware
app.use(bodyParser.json());


//Use routes
app.use('/users', users);
app.use('/cars', cars);
app.use('/travels', travels);
app.use('/travel-types', travelTypes);


const port = 5000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});




