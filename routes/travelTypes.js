const _ = require('lodash');
const express = require('express');
const {ObjectID} = require('mongodb');
const mongoose = require('mongoose');
const router = express.Router();

//Authentication middleware
const {authenticate} = require('../helpers/auth');

//Load TravelType Model
require('../models/TravelType');
const TravelType = mongoose.model('travelTypes');

router.get('/', authenticate, (req, res) => {
  TravelType.find({
  })
      .sort('distanceMax')
    .then((travelType) => {
    res.send(travelType)
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/distance/:distance', (req, res) => {
    TravelType.find({
        distanceMax: { $gt: req.params.distance }
    }).sort('distanceMax')
        .limit(1)
        .then((travelType) => {
            res.send(travelType);
        }).catch(e => {
        res.status(400).send(e);
    })
});

router.post('/', authenticate, (req, res) => {
  let body = _.pick(req.body, ['price','distanceMax']);
  let travel = new TravelType(body);
  if (req.user.role !== 'admin') {
    return res.status(401).send();
  } else {
    travel.save().then((doc) => {
      res.send(doc);
    }).catch(e => {
      res.status(400).send(e);
    })
  }
});

router.get('/:id', (req, res) => {
    TravelType.findOne({
        _id: req.params.id,
    })
        .then((travelType) => {
            if (!travelType) {
                return res.status(404).send();
            }
            res.send({travelType});
        }).catch(e => {
        res.status(400).send(e);
    })
});



router.patch('/edit/:id', authenticate, (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ['price']);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send();
  }
  if (req.user.role !== 'admin') {
    return res.status(401).send();
  }
  TravelType.findOneAndUpdate({
      _id: id
    },
    {$set: body},
    {new: true}).then((travelType) => {
    if (!travelType) {
      return res.status(404).send();
    }
    res.send({travelType});
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.delete('/:id', authenticate, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(400).send();
  }
  if (req.user.role !== 'admin') {
    return res.status(401).send();
  }

  TravelType.findOneAndRemove({
    _id: id
  }).then((travelType) => {
    if (!travelType) {
      return res.status(404).send();
    }
    res.send({travelType});
  }).catch(e => {
    res.status(400).send(e);
  })
});


module.exports = router;