//test commit

const _ = require('lodash');
const express = require('express');
const {ObjectID} = require('mongodb');
const mongoose = require('mongoose');
const router = express.Router();

//Authentication middleware
const {authenticate} = require('../helpers/auth');

//Load Car Model
require('../models/Car');
const Car = mongoose.model('cars');

router.get('/', authenticate, (req, res) => {
  Car.find({
    user: req.user._id
  }).populate('user')
    .then((cars) => {
    res.send(cars);
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.post('/', authenticate, (req, res) => {
  let body = _.pick(req.body, ['model', 'make', 'seating', 'licensePlate']);
  body.user = req.user._id;
  let car = new Car(body);
  car.save().then((doc) => {
    res.send(doc);
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/:id', authenticate, (req, res) => {
  Car.findOne({
    _id: req.params.id,
    user: req.user._id
  }).populate('user')
    .then((car) => {
    if (!car) {
      return res.status(404).send();
    }
    res.send({car});
  }).catch(e => {
    res.status(400).send(e);
  })
});


router.get('/:lp', authenticate, (req, res) => {
    Car.findOne({
        licensePlate: req.params.lp
    })
        .then((car) => {
            if (!car) {
                return res.status(404).send();
            }
            res.send({car});
        }).catch(e => {
        res.status(400).send(e);
    })
});

router.patch('/:id', authenticate, (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ['model', 'make', 'seating', 'licensePlate']);

  if (!ObjectID.isValid(id)) {
    return res.status(400).send();
  }
  Car.findOneAndUpdate({
      _id: id,
      user: req.user._id
    },
    {$set: body},
    {new: true}).then((car) => {
    if (!car) {
      return res.status(404).send();
    }
    res.send({car});
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.delete('/:id', authenticate, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(400).send();
  }
  Car.findOneAndRemove({
    _id: id,
    user: req.user._id
  }).then((car) => {
    if (!car) {
      return res.status(404).send();
    }
    res.send({car});
  }).catch(e => {
    res.status(400).send(e);
  })
});

module.exports = router;