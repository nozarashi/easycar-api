const _ = require('lodash');
const express = require('express');
const {ObjectID} = require('mongodb');
const mongoose = require('mongoose');
const router = express.Router();

//Authentication middleware
const {authenticate} = require('../helpers/auth');

//Load Travel Model
require('../models/Travel');
const Travel = mongoose.model('travels');

//Load Car Model
require('../models/Car');
const Car = mongoose.model('cars');

//Load User Model
require('../models/User');
const User = mongoose.model('users');

router.get('/', (req, res) => {
  Travel.find({
    user: req.user._id
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.post('/comment/:id', authenticate, (req, res) => {
  Travel.findOne({
    _id: req.params.id
  }).then(travel => {
    const newComment = {
      commentBody: req.body.commentBody,
      rating: req.body.rating,
      commentUser: req.user._id
    };

    travel.comments.unshift(newComment);
    travel.save()
      .then(travel => {
        User.findOne({
          _id: travel.user
        }).then(user => {
          if (!req.user._id.equals(travel.user)) { // pas besoin de changer la note si c'est le conducteur qui commente
            user.rating = (user.rating === -1 ? req.body.rating : (user.rating + req.body.rating) / 2);
          }
          user.save().then(user => {
            if (req.body.rating < 3) {
              const notification = {
                name: `${user.firstName} ${user.lastName}`,
                photo: user.photo,
                message: `a reçu une mauvaise évaluation (${req.body.rating})`,
                target: req.params.id
              };
              User.update(
                {role: 'admin'},
                {$push: {notifications: notification}},
                {multi: true}).then(users => {
              }).catch(e => {
                res.status(400).send(e);
              });
            }

            res.send({travel})
          }).catch(e => {
            res.status(400).send(e)
          })
        }).catch(e => {
          res.status(400).send(e)
        });
      }).catch(e => {
      res.status(400).send(e)
    })
      .catch(e => {
        res.status(400).send(e)
      })
  })
});

router.post('/', authenticate, (req, res) => {
  let body = _.pick(req.body, ['departure', 'destination', 'departureAddress', 'destinationAddress', 'price', 'date', 'placesAvailable', 'car', 'averageTime', 'distance']);
  body.user = req.user._id;
  let travel = new Travel(body);
  if (!req.user.isActive) {
    return res.status(401).send('Votre compte est suspendu')
  }
  Car.findOne({
    _id: travel.car, user: req.user._id
  }).then((car) => {
    if (!car) {
      return res.status(404).send();
    }
    if (car.seating <= travel.placesAvailable) {
      travel.placesAvailable = car.seating - 1;
    }
  });
  travel.save().then((doc) => {
    res.send(doc);
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/allUser', authenticate, (req, res) => {
  Travel.find({
    user: req.user._id
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/allUserAfter', authenticate, (req, res) => {
  Travel.find({
    user: req.user._id,
    date: {$gte: new Date()}
  }).sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

//Prochains voyages en tant que passager
router.get('/asPassenger', authenticate, (req, res) => {
  Travel.find({
    passengers: req.user._id,
    date: {$gte: new Date()}
  }).sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

//Historique en tant que passager
router.get('/history-asPassenger', authenticate, (req, res) => {
  Travel.find({
    passengers: req.user._id,
    date: {$lte: new Date()}
  }).sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});


//Réserver une place
router.post('/book/:id', authenticate, (req, res) => {
  Travel.findOne({
    _id: req.params.id
  }).then((travel) => {
    //ajout à la liste des passagers
    if (travel.placesAvailable > 0) {
      travel.passengers.unshift(req.user._id);
      travel.placesAvailable -= 1;

      travel.save()
        .then(travel => {
          res.send({travel})
        }).catch(e => {
        res.status(400).send(e);
      });
    } else {
      return res.status(400).send(e);
    }
  }).catch(e => {
    res.status(401).send(e)
  })
});

router.delete('/book/:id', authenticate, (req, res) => {
  Travel.findOneAndUpdate({
      _id: req.params.id
    },
    {
      $pull: {passengers: req.user._id},
      $inc: {placesAvailable: 1}
    }
  ).then((travel) => {
    res.send({travel});
  }).catch(e => {
    res.status(400).send(e)
  })
});

router.get('/all', (req, res) => {
  Travel.find({})
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/allafter', (req, res) => {
  Travel.find({
    date: {$gte: new Date()}
  }).sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});


router.get('/specificAfterDate/:departure/:destination/:start', (req, res) => {
  Travel.find({
    departure: req.params.departure,
    destination: req.params.destination,
    date: {$gte: req.params.start}
  }).sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});


router.get('/specificAfter/:departure/:destination', (req, res) => {
  Travel.find({
    departure: req.params.departure,
    destination: req.params.destination,
    date: {$gte: new Date()}
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/specificInbetween/:departure/:destination/:start/:end', (req, res) => {
  Travel.find({
    departure: req.params.departure,
    destination: req.params.destination,
    $and: [{date: {$lte: req.params.end}}, {date: {$gte: req.params.start}}]
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/departure/:departure', (req, res) => {
  Travel.find({
    departure: req.params.departure
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/departureAfter/:departure', (req, res) => {
  Travel.find({
    departure: req.params.departure,
    date: {$gte: new Date()}
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/departureAfterDate/:departure/:start', (req, res) => {
  Travel.find({
    departure: req.params.departure,
    date: {$gte: req.params.start}
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/departureInbetween/:departure/:start/:end', (req, res) => {
  Travel.find({
    departure: req.params.departure,
    $and: [{date: {$lte: req.params.end}}, {date: {$gte: req.params.start}}]
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/destination/:destination', (req, res) => {
  Travel.find({
    destination: req.params.destination
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/destinationAfter/:destination', (req, res) => {
  Travel.find({
    destination: req.params.destination,
    date: {$gte: new Date()}
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/destinationAfterDate/:destination/:start', (req, res) => {
  Travel.find({
    destination: req.params.destination,
    date: {$gte: req.params.start}
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/date/:date', (req, res) => {
  Travel.find({
    date: req.params.date
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/dateafter/:date', (req, res) => {
  Travel.find({
    date: {$gte: req.params.date}
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travels) => {
      res.send(travels);
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/inbetween/:start/:end', (req, res) => {
  Travel.find({
    $and: [{date: {$lte: req.params.end}}, {date: {$gte: req.params.start}}]
  })
    .sort({date: 'asc'})
    .populate('user')
    .populate('car')
    .then((travel) => {
      if (!travel) {
        return res.status(404).send();
      }
      res.send({travel});
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/travels-per-month', authenticate, (req, res) => {
  if (req.user.role !== 'admin') {
    return res.send(401).send();
  }
  let d = new Date();
  d.setMonth(d.getMonth() - 6);

  Travel.aggregate(
    [
      {$match: {date: {$gte: d}}},

      {
        $group: {
          _id: {month: {$month: "$date"}, year: {$year: "$date"}},
          count: {$sum: 1}
        }
      }
    ]
  ).then(data => {
    res.send(data);
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/popular-travels', authenticate, (req, res) => {
  if (req.user.role !== 'admin') {
    return res.send(401).send();
  }
  let d = new Date();
  d.setMonth(d.getMonth() - 6);

  Travel.aggregate(
    [
      {$match: {date: {$gte: d}}},

      {
        $group: {
          _id: {departure: "$departure", destination: "$destination"},
          count: {$sum: 1}
        }
      },
      {$sort: {count: -1}},
      {$limit: 5}
    ]
  ).then(data => {
    res.send(data);
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.get('/:id', (req, res) => {
  Travel.findOne({
    _id: req.params.id,
    //user: req.user._id
  })
    .populate('user')
    .populate('car')
    .populate('passengers')
    .populate('comments.commentUser')
    .then((travel) => {
      if (!travel) {
        return res.status(404).send();
      }
      res.send({travel});
    }).catch(e => {
    res.status(400).send(e);
  })
});

router.patch('/edit/:id', authenticate, (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ['departure', 'destination', 'departureAddress', 'destinationAddress', 'price', 'date', 'placesAvailable', 'car', 'averageTime', 'distance']);
  if (!ObjectID.isValid(id)) {
    return res.status(400).send();
  }
  Travel.findOneAndUpdate({
      _id: id,
      user: req.user._id
    },
    {$set: body},
    {new: true}).then((travel) => {
    if (!travel) {
      return res.status(404).send();
    }
    res.send({travel});
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.delete('/:id', authenticate, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(400).send();
  }
  Travel.findOneAndUpdate({
      _id: id,
      user: req.user._id
    },
    {$set: {isOnline: false}},
    {new: true}).then((travel) => {
    if (!travel) {
      return res.status(404).send();
    }
    res.send({travel});
  }).catch(e => {
    res.status(400).send(e);
  })
});

router.delete('/undelete/:id', authenticate, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(400).send();
  }
  Travel.findOneAndUpdate({
      _id: id,
      user: req.user._id
    },
    {$set: {isOnline: true}},
    {new: true}).then((travel) => {
    if (!travel) {
      return res.status(404).send();
    }
    res.send({travel});
  }).catch(e => {
    res.status(400).send(e);
  })
});

module.exports = router;