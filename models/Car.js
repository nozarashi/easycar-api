/**
 * Created by thibaultnoll on 11/10/17...
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'users'
  },
  model: {
    type: String,
    required: true
  },
  make: {
    type: String,
    required: true
  },
  seating: {
    type: Number,
    required: true
  },
  licensePlate: {
    type: String,
    required: true,
    unique: true
  }
});

mongoose.model('cars', CarSchema);