/**
 * Created by thibaultnoll on 11/10/17.
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TravelTypeSchema = new Schema({

  price: {
    type: Number,
    required: true
  },
  distanceMax: {
    type: Number,
    required: true
  },
});

mongoose.model('travelTypes', TravelTypeSchema);

